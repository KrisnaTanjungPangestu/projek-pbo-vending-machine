package com.company;

import java.util.Scanner;

public class vendingmachine implements Daftar{
    Scanner b = new Scanner(System.in);
    Produk pr = new Produk();
    private int pilih, uang;

    public int getPilih() {
        return pilih;
    }

    public void setPilih(int pilih) {
        this.pilih = pilih;
    }

    public int getUang() {
        return uang;
    }

    public void setUang(int uang) {
        this.uang = uang;
    }

    @Override
    public void menu() {
        System.out.println("\t\t\t\t============Daftar Menu============");
        System.out.println("| No | \t\tProduk\t\t|\t\tHarga\t\t|\t\tStok\t|\t");
        for (int i = 0; i < pr.produk.length; i++) {
            System.out.println("| " + (i + 1) + "  |" + " " + pr.produk[i] + "\t\t\t|\t\t" + pr.harga[i] + "\t\t|" + "\t\t" + pr.stock[i] + "\t\t|\n");
        }
    }

    @Override
    public void pilihproduk() {
        if (pilih == 1) {
            System.out.print("Masukkan Uang : Rp ");
            uang = b.nextInt();
            if (pr.stock[pilih - 1] == 0) {
                System.out.println("Maaf Produk Kosong");
                System.out.println("Silahkan Ambil Uang Anda Rp " + uang);
            } else {
                if (pilih == 1 && uang == 20000) {
                    System.out.println();
                    System.out.println("=====Pembayaran Berhasil=====");
                    System.out.println("Nama : " + pr.produk[pilih - 1]);
                    System.out.println("Harga : " + pr.harga[pilih - 1]);
                    System.out.println("Jumlah Uang : " + uang);
                    System.out.println("Kembalian : " + (pr.harga[pilih - 1] - uang));
                    System.out.println("Terima Kasih");
                    pr.stock[pilih - 1] -= 1;
                }else if (uang < pr.harga[pilih-1]){
                    System.out.println("Maaf uang anda tidak cukup");
                    System.out.println("Silahkan Ambil Uang Anda Rp " + uang);
                }else{
                    System.out.println("Maaf Uang harus pecahan 20000 || 10000 || 5000");
                    System.out.println("Silahkan Ambil Uang Anda Rp " + uang);
                }
            }
        }else if (pilih == 2){
            System.out.print("Masukkan Uang : Rp. ");
            uang = b.nextInt();
            if (pr.stock[pilih - 1] == 0) {
                System.out.println("Maaf Produk Kosong");
                System.out.println("Silahkan Ambil Uang Anda Rp " + uang);
            } else {
                if (pilih == 2 && uang == 20000) {
                    System.out.println();
                    System.out.println("=====Pembayaran Berhasil=====");
                    System.out.println("Nama : " + pr.produk[pilih - 1]);
                    System.out.println("Harga : " + pr.harga[pilih - 1]);
                    System.out.println("Jumlah Uang : " + uang);
                    System.out.println("Kembalian : " + (pr.harga[pilih - 1] - uang));
                    System.out.println("Terima Kasih");
                    pr.stock[pilih - 1] -= 1;
                }else if (uang < pr.harga[pilih-1]){
                    System.out.println("Maaf uang anda tidak cukup");
                    System.out.println("Silahkan Ambil Uang Anda Rp " + uang);
                } else {
                    System.out.println("Maaf Uang harus pecahan 20000 || 10000 || 5000");
                    System.out.println("Silahkan Ambil Uang Anda Rp " + uang);
                }
            }
        }else if (pilih == 3){
            System.out.print("Masukkan Uang : Rp. ");
            uang = b.nextInt();
            if (pr.stock[pilih - 1] == 0) {
                System.out.println("Maaf Produk Kosong");
                System.out.println("Silahkan Ambil Uang Anda Rp " + uang);
            } else {
                if (pilih == 3 && uang == 20000 || uang == 10000) {
                    System.out.println();
                    System.out.println("=====Pembayaran Berhasil=====");
                    System.out.println("Nama : " + pr.produk[pilih - 1]);
                    System.out.println("Harga : " + pr.harga[pilih - 1]);
                    System.out.println("Jumlah Uang : " + uang);
                    System.out.println("Kembalian : " + (pr.harga[pilih - 1] - uang));
                    System.out.println("Terima Kasih");
                    pr.stock[pilih - 1] -= 1;
                }else if (uang < pr.harga[pilih-1]){
                    System.out.println("Maaf uang anda tidak cukup");
                    System.out.println("Silahkan Ambil Uang Anda Rp " + uang);
                } else {
                    System.out.println("Maaf Uang harus pecahan 20000 || 10000 || 5000");
                    System.out.println("Silahkan Ambil Uang Anda Rp " + uang);
                }
            }
        }else if (pilih == 4){
            System.out.print("Masukkan Uang : Rp. ");
            uang = b.nextInt();
            if (pr.stock[pilih - 1] == 0) {
                System.out.println("Maaf Produk Kosong");
                System.out.println("Silahkan Ambil Uang Anda Rp " + uang);
            } else {
                if (pilih == 4 && uang == 20000 || uang == 10000 || uang == 5000) {
                    System.out.println();
                    System.out.println("=====Pembayaran Berhasil=====");
                    System.out.println("Nama : " + pr.produk[pilih - 1]);
                    System.out.println("Harga : " + pr.harga[pilih - 1]);
                    System.out.println("Jumlah Uang : " + uang);
                    System.out.println("Kembalian : " + (pr.harga[pilih - 1] - uang));
                    System.out.println("Terima Kasih");
                    pr.stock[pilih - 1] -= 1;
                }else if (uang < pr.harga[pilih-1]){
                    System.out.println("Maaf uang anda tidak cukup");
                    System.out.println("Silahkan Ambil Uang Anda Rp " + uang);
                } else {
                    System.out.println("Maaf Uang harus pecahan 20000 || 10000 || 5000");
                    System.out.println("Silahkan Ambil Uang Anda Rp " + uang);
                }
            }
        }else{
            System.out.println("Menu Tidak Tersedia!");
        }
    }
}