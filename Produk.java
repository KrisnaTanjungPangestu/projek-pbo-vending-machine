package com.company;

public class Produk {
    String produk[] = {"Chitato", "Qtela", "Potabee", "Doritos"};
    int harga[] = {20000, 15000, 10000, 5000};
    int stock[] = {3, 5, 7, 4};

    public String[] getProduk() {
        return produk;
    }

    public void setProduk(String[] produk) {
        this.produk = produk;
    }

    public int[] getHarga() {
        return harga;
    }

    public void setHarga(int[] harga) {
        this.harga = harga;
    }

    public int[] getStock() {
        return stock;
    }

    public void setStock(int[] stock) {
        this.stock = stock;
    }
}